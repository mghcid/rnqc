"""
Project: htqc
File: setup


Contact:
---------
salturki@gmail.com

10:41 
5/7/15
"""
__author__ = 'Saeed Al Turki'

from setuptools import setup, find_packages


def load_requirements():
    f = open('requirements.txt')
    return [l.strip(' ') for l in f]

setup(
    name='rnqc',
    version=1.2,
    py_modules=['rnqc.rnqc_v2'],
    packages=find_packages(),
    include_package_data=True,
    package_data={'rnqc': ['*.txt']},
    url='https://bitbucket.org/salturki/rnqc',
    author='Saeed Al Turki',
    author_email='salturki@gmail.com',
    license='MIT',
    install_requires=load_requirements(),
    entry_points='''
        [console_scripts]
        rnqc=rnqc.rnqc_v2:cli
    ''',
)
