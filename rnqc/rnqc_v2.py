"""
Project: rnqc
File: rnqc_v2


Contact:
---------
salturki@gmail.com

11:37 
4/3/15
"""
__author__ = 'Saeed Al Turki'

import click
import pysam
from copy import deepcopy
from bx.intervals.intersection import Intersecter, Interval

GLOBAL_STATS = {
    'total_tags': {"dup": 0, "nondup": 0},
    'total_reads': {"dup": 0, "nondup": 0},
    'read1': {"dup": 0, "nondup": 0},
    'read2': {"dup": 0, "nondup": 0},
    'mapped': {"dup": 0, "nondup": 0},
    'unmapped': {"dup": 0, "nondup": 0},
    'split': {"dup": 0, "nondup": 0},
    'unsplit': {"dup": 0, "nondup": 0},
    'x-x': {"dup": 0, "nondup": 0},  # chrom > 24
    'u-u': {"dup": 0, "nondup": 0},
    'dna': {"dup": 0, "nondup": 0},
    'rna': {"dup": 0, "nondup": 0},
    'rna/dna': {"dup": 0, "nondup": 0},
}

JUNCTION_COMBINATIONS = {}  # holder dict to be copied for every gene
for i in ['e', 'i', 'j']:
    for j in ['e', 'i', 'j']:
        key = "%s-%s" % (i, j)
        JUNCTION_COMBINATIONS[key] = {"dup": 0, "nondup": 0}
        GLOBAL_STATS[key] = {"dup": 0, "nondup": 0}


DETAILED_STATS = {}
TARGET_GENES = []
GENE_MODELS = {}
TRANSCRIPTS = {}
JUNCTIONS = {}


def convert_chrom(chrom):
    if chrom == 23:
        chrom = 'X'
    elif chrom == 24:
        chrom = 'Y'
    return str(chrom)


def nice_percentage_string(n, d):
    if d == 0:
        return 0
    return "%.2f%%" % round((n / float(d)) * 100, 2)


def get_read_junction(reference_id, r_start, r_end):
    r_start_place = 'u'
    r_end_place = 'u'

    if reference_id > 23:
        return 'x-x'

    chrom = convert_chrom(reference_id + 1)

    ts = TRANSCRIPTS[chrom].find(r_start, r_end)

    for t in ts:
        exons = GENE_MODELS[chrom][(t.start, t.end)]['exons'].find(r_start, r_start)
        if r_start in JUNCTIONS[chrom]:
            r_start_place = 'j'
        elif exons:
            r_start_place = 'e'
        else:
            r_start_place = 'i'

        exons = GENE_MODELS[chrom][(t.start, t.end)]['exons'].find(r_end, r_end)
        if r_end in JUNCTIONS[chrom]:
            r_end_place = 'j'
        elif exons:
            r_end_place = 'e'
        else:
            r_end_place = 'i'

    read_place = "%s-%s" % (r_start_place, r_end_place)

    return read_place


def sum_up_dna_rna_counts():
    #RNA
    for item in ['j-e', 'e-j', 'j-j']:
        for dup_tag in ['dup', 'nondup']:
            GLOBAL_STATS['rna'][dup_tag] += GLOBAL_STATS[item][dup_tag]

    #DNA
    for item in ['e-i', 'i-e', 'i-i', 'i-j', 'j-i']:
        for dup_tag in ['dup', 'nondup']:
            GLOBAL_STATS['dna'][dup_tag] += GLOBAL_STATS[item][dup_tag]

    #RNA/DNA
    for dup_tag in ['dup', 'nondup']:
        GLOBAL_STATS['rna/dna'][dup_tag] = 0 if float(GLOBAL_STATS['dna'][dup_tag]) == 0 else \
            round(GLOBAL_STATS['rna'][dup_tag] / float(GLOBAL_STATS['dna'][dup_tag]), 2)

def get_rna_dna_ratio_total():
    nom = GLOBAL_STATS['rna']['dup'] + GLOBAL_STATS['rna']['nondup']
    dom = GLOBAL_STATS['dna']['dup'] + GLOBAL_STATS['dna']['nondup']
    if dom > 0:
        return round(nom / float(dom), 2)
    else:
        return 0

def bam_stat(input_bam):
    # print "BAM stats .."

    bam = pysam.AlignmentFile(input_bam)
    previous_read_name = None
    read1 = None
    read2 = None
    for read in bam.fetch(until_eof=True):

        if read.is_duplicate:
            dup_tag = 'dup'
        else:
            dup_tag = 'nondup'

        if read.qname == previous_read_name and read.is_read1 == read1 and read.is_read2 == read2:
            pass
        else:
            GLOBAL_STATS['total_reads'][dup_tag] += 1
            if not read.is_unmapped:
                if read.has_tag('SA'):
                    # if read.is_secondary:
                    GLOBAL_STATS['split'][dup_tag] += 1
                else:
                    GLOBAL_STATS['unsplit'][dup_tag] += 1

            if read.is_unmapped:
                GLOBAL_STATS['unmapped'][dup_tag] += 1
            else:
                GLOBAL_STATS['mapped'][dup_tag] += 1

            if read.is_read1:
                GLOBAL_STATS['read1'][dup_tag] += 1

            if read.is_read2:
                GLOBAL_STATS['read2'][dup_tag] += 1

        previous_read_name = read.qname
        read1 = read.is_read1
        read2 = read.is_read2

        if not read.is_unmapped:# and read.has_tag('SA'):
            read_junction = get_read_junction(read.reference_id, read.reference_start, read.reference_end)
            GLOBAL_STATS[read_junction][dup_tag] += 1
            # if read_junction not in ['u-u', 'x-x']:
            GLOBAL_STATS['total_tags'][dup_tag] += 1


    print "#### GLOBAL STATS #####"
    print "\t".join(["#GROUP", "NON-DUP", "NON-DUP%", "DUP", "DUP%", "TOTAL", "TOTAL%"])
    items = ['total_reads', 'read1', 'read2', 'mapped', 'unmapped', 'split', 'unsplit']#, 'dna', 'rna']
    for key in items:
        sum_count = GLOBAL_STATS[key]['dup'] + GLOBAL_STATS[key]['nondup']
        print "\t".join([str(x) for x in [key,
                                          GLOBAL_STATS[key]['nondup'],
                                          nice_percentage_string(GLOBAL_STATS[key]['nondup'],
                                                                 GLOBAL_STATS['total_reads']['nondup']),
                                          GLOBAL_STATS[key]['dup'],
                                          nice_percentage_string(GLOBAL_STATS[key]['dup'],
                                                                 GLOBAL_STATS['total_reads']['dup']),
                                          sum_count,
                                          nice_percentage_string(sum_count,
                                                                 GLOBAL_STATS['total_reads']['dup'] +
                                                                 GLOBAL_STATS['total_reads']['nondup']),
                                          ]])

    print "\n\n#### JUNCTION STATS ####"
    print "\t".join(["#GROUP", "NON-DUP", "NON-DUP%", "DUP", "DUP%", "TOTAL", "TOTAL%"])
    sum_up_dna_rna_counts()
    items = ['rna', 'dna', 'rna/dna', 'u-u', 'x-x']
    items.extend(sorted(JUNCTION_COMBINATIONS.keys()))
    for key in items:
        sum_count = GLOBAL_STATS[key]['dup'] + GLOBAL_STATS[key]['nondup']
        if key != 'rna/dna':
            print "\t".join([str(x) for x in [key,
                                              GLOBAL_STATS[key]['nondup'],
                                              nice_percentage_string(GLOBAL_STATS[key]['nondup'],
                                                                     GLOBAL_STATS['total_tags']['nondup']),
                                              GLOBAL_STATS[key]['dup'],
                                              nice_percentage_string(GLOBAL_STATS[key]['dup'],
                                                                     GLOBAL_STATS['total_tags']['dup']),
                                              sum_count,
                                              nice_percentage_string(sum_count,
                                                                     GLOBAL_STATS['total_tags']['dup'] +
                                                                     GLOBAL_STATS['total_tags']['nondup']),
                                              ]])
        else:
            print "\t".join([str(x) for x in [key,
                                              GLOBAL_STATS[key]['nondup'],
                                              "-",
                                              GLOBAL_STATS[key]['dup'],
                                              "-",
                                              get_rna_dna_ratio_total(),
                                              "-",
                                              ]])

    print "\n\n#### GENE STATS #####"


def load_target_genes(path):
    holder = []
    f = open(path, 'r')
    for line in f:
        if line.startswith("#"):
            continue
        holder.append(line.strip())
    return holder


def load_gene_models(ref_gene_path, target_gene_path):
    if target_gene_path:
        TARGET_GENES = load_target_genes(target_gene_path)
    else:
        TARGET_GENES = None

    f = open(ref_gene_path, 'r')
    for line in f:
        if line.startswith('#'):
            header = line.strip().split("\t")
            continue

        line = line.strip().replace('"', '').split("\t")

        if not line[1].startswith('NM_'):
            continue

        chrom, t_start, t_end, gene_name = line[2], int(line[4]), int(line[5]), line[12]

        if TARGET_GENES is not None:
            if gene_name not in TARGET_GENES:
                continue

        if chrom not in GENE_MODELS:
            GENE_MODELS[chrom] = {}
            TRANSCRIPTS[chrom] = Intersecter()

        TRANSCRIPTS[chrom].add_interval(Interval(t_start, t_end))
        GENE_MODELS[chrom][(t_start, t_end)] = {"transcript_id": line[3],
                                           "gene_name": gene_name,
                                           'exons': Intersecter()}
        exon_starts = [int(x) for x in line[9].split(",")[:-1]]
        exon_ends = [int(x) for x in line[10].split(",")[:-1]]

        # load exon junctions
        if chrom not in JUNCTIONS:
            JUNCTIONS[chrom] = {}

        # exon_ends[-1] = int(line[7])  # to avoid UTR
        exons = zip(exon_starts, exon_ends)
        for exon in exons:
            GENE_MODELS[chrom][(t_start, t_end)]['exons'].add_interval(Interval(exon[0], exon[1]))
            for i in range(1, 4):  # add -/0
                JUNCTIONS[chrom][exon[0] + i] = ''
                JUNCTIONS[chrom][exon[0] - i] = ''
                JUNCTIONS[chrom][exon[1] + i] = ''
                JUNCTIONS[chrom][exon[1] - i] = ''

        # create place holder
        if gene_name not in DETAILED_STATS:
            DETAILED_STATS[gene_name] = deepcopy(JUNCTION_COMBINATIONS)

    f.close()


@click.group(invoke_without_command=True)
@click.option('--input-bam', '-i', help='Input file (bam)')
@click.option('--gene-model', '-g', help='Gene model in a bed format')
@click.option('--target-genes', '-t', help='Text file of target gene HUGO symbols (one per line)')
def cli(input_bam, gene_model, target_genes):
    """A QC tool for CID RNA-Seq bam files"""
    if input_bam and gene_model:
        load_gene_models(gene_model, target_genes)
        bam_stat(input_bam)
    else:
        click.secho('use --help to show options', fg='white')

if __name__ == '__main__':
    cli()
